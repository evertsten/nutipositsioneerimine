import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { MonitoringComponent } from './monitoring/monitoring.component';
import { StudiesComponent } from './studies/studies.component';
import { UsersComponent } from './users/users.component';
import { QuestionsComponent } from './questions/questions.component';
import { StaffComponent } from './staff/staff.component';
import { NavigationComponent } from './navigation/navigation.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatMenuModule } from '@angular/material/menu';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSortModule } from '@angular/material/sort';
import { MatSelectModule } from '@angular/material/select';
import { FormsModule } from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { SocketioService } from './services/socketio.service';

const routes: Routes = [
  { path: '', component: MonitoringComponent },
  { path: 'studies', component: StudiesComponent },
  { path: 'users', component: UsersComponent },
  { path: 'questions', component: QuestionsComponent },
  { path: 'staff', component: StaffComponent },
  //{ path: 'user/create-password', component: CreatePasswordComponent, canActivate: [AuthGuard, RoleGuard] },
  //{ path: 'user/invitation-expired/:invitationId', component: InvitationExpiredComponent },
  // { path: 'confirm-email/:token', component: EmailConfirmationComponent }, // EMAIL IS CONFIRMED IN THE BACKEND
  { path: 'login', component: LoginComponent },
  // { path: 'signup', component: SignupComponent }, // no signup, only invitations by admins
  { path: '**', redirectTo: '/login' },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MonitoringComponent,
    StudiesComponent,
    UsersComponent,
    QuestionsComponent,
    NavigationComponent,
    StaffComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    MatTableModule,
    BrowserAnimationsModule,
    MatMenuModule,
    MatButtonModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
    MatSelectModule,
    FormsModule,
    MatProgressSpinnerModule,
    RouterModule.forRoot(routes, {
      onSameUrlNavigation: 'reload'
    })
  ],
  providers: [SocketioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
