import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({providedIn: 'root'})
export class HttpService {
  static get<T>(arg0: string) {
      throw new Error('Method not implemented.');
  }
  constructor(private httpClient: HttpClient) {}

  public get<T = any>(endpoint: string): Promise<T> {
    return new Promise<T>(((resolve, reject) => {
      this.httpClient.get<T>(environment.apiUrl + endpoint)
        .subscribe(result => resolve(result),
          error => reject(error));
    }));
  }

  public post<T = any>(endpoint: string, body: any): Promise<T> {
    return new Promise<T>(((resolve, reject) => {
      this.httpClient.post<T>(environment.apiUrl + endpoint, body)
        .subscribe(result => resolve(result),
          error => reject(error));
    }));
  }
}
