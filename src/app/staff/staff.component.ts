import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {HttpService} from '../http/http.service';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { SocketioService } from "../services/socketio.service";

export interface UserData {
  id: number;
  email: string;
  last_login: Date;
  studies: string;
  is_active:boolean;
  is_staff:boolean;
  is_admin:boolean;
  editMode: boolean;
}

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent implements OnInit, AfterViewInit {
  @ViewChild(MatSort) sort: MatSort;


  displayedColumns: string[] = ['id', 'email', 'last_login', 'studies', 'is_active', 'role', 'edit', 'delete'];
  dataSource: MatTableDataSource<UserData>;

  constructor(private http: HttpService, private socketService: SocketioService) {
    this.dataSource = new MatTableDataSource();
   };

  editMode = false;
  addNewMode = false;
  isLoading = true;
  
  users;
  userData = [];

  msg;

  newStudies = [];
  newRole;
  newStudy;
  newStatus;
  newEmail;


  async ngOnInit(): Promise<void> {

    try{
      this.users = await this.http.get('api/staff/getAll');
      for (const user of this.users) {
        this.userData.push({
          id: user.id,
          email: user.email,
          last_login: user.last_login,
          studies: user.studies,
          is_active: user.is_active,
          is_staff: user.is_staff,
          is_admin: user.is_admin,
          editMode: false
        })
      }
      this.dataSource = new MatTableDataSource(this.userData);
      this.dataSource.sort = this.sort;
    }catch(e){
      console.error('User error', e);
    }
    
    this.isLoading = false;
  }

  sendMsg(msg){
    this.socketService.socket.emit('message', msg);
  };

  editStaffMode(user){
    user.editMode = true;
  };

  cancelEditStaffMode(user){
    user.editMode = false;
  };

  addStaffMode(){
    this.addNewMode = true;
  };

  addStaffModeCancel(){
    this.addNewMode = false;
  }

  cancelAddStaffMode(){
    this.addNewMode = false;
  };

  async addstaff(newStudy, newStatus, newRole, newEmail){
    document.getElementById('body').style.opacity = "0.1";

    this.newStudies.push(newStudy);
    const status = newStatus;
    const role = newRole;
    const email = newEmail;

    const payload =
    {
      "email": email,
      "role": role,
      "studies": this.newStudies,
      "status": status
    }
    console.log(payload)

    await this.http.post("api/staff/new", payload);

    document.getElementById('body').style.opacity = "1";
  }

  async updateStaff(user){
    document.getElementById('body').style.opacity = "0.1";

    const id = user.id;
    const email = user.email;
    const last_login = user.last_login;
    this.newStudies.push(user.studies);
    const is_active = user.is_active;
    const role = user.role;

    const payload =
    {
      "id": id,
      "email": email,
      "last_login": last_login,
      "studies": this.newStudies,
      "is_active": is_active,
      "role": role
    }
    console.log(payload)
    await this.http.post("api/staff/updateOne", payload);

    user.editMode = false;

    document.getElementById('body').style.opacity = "1";
  }; 

  async deleteStaff(user){
    document.getElementById('body').style.opacity = "0.1";

    const id = user.id;

    const payload =
    {
      "id": id
    }

    await this.http.post("api/staff/deleteOne", payload);

    user.editMode = false;
    document.getElementById('body').style.opacity = "1";
  };

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  };

  ngAfterViewInit() {
  };

};
