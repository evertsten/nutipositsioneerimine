import { Component, OnInit } from '@angular/core';
import { SocketioService } from "./services/socketio.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'nutipositsioneerimine';

  constructor(private socketService: SocketioService) {};

  SocketConnected = false;

  async ngOnInit(): Promise<void>{
    if(this.SocketConnected === false){
      this.socketService.setupSocketConnection();
      this.socketService.socket.emit('newConnection');
      this.SocketConnected = true;
    }
    this.socketService.socket.on('broadcast', (data: string) => {
      console.log(data);
    });
  }
}

