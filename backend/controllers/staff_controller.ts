import {NextFunction, Request, response, Response} from 'express';
import HttpException from '../exceptions/http_exception';
import {HttpStatusCodes} from '../shared/http_status_codes.shared';
import Staff from '../models/staff';

export const allStaff = async (req: Request, res: Response, next: NextFunction) => {
  // Find all staff
  try{
    const allStaff = await Staff.findAll({
      attributes: ['id', 'email', 'password', 'last_login', 'studies', 'is_active', 'is_staff', 'is_admin']
    });
    res.json(allStaff);
  } catch (e) {
    console.error('Find all staff error', e);
    next(new HttpException(HttpStatusCodes.InternalServerError, e));
    res.send({status: 'error'})
  }
};

export const updateStaff = async (req: Request, res: Response, next: NextFunction) => {
  // Find & edit staff member
  const {id, email, last_login, studies, is_active} = req.body;
  let role = req.body.role;
  let is_admin;
  let is_staff;

  if(role === "admin"){
    is_admin = true;
    is_staff = false;
  } else if(role === "staff"){
    is_staff = true;
    is_admin = false;
  };

  try{
    await Staff.update(
      {email:email, last_login:last_login, studies:studies, is_active:is_active, is_staff:is_staff, is_admin: is_admin}, {where: {id:id}}
    )
    res.send({status: 'ok'})
  } catch (e) {
    console.error('Update staff error', e);
    next(new HttpException(HttpStatusCodes.InternalServerError, e));
    res.send({status: 'error'})
  }
};

export const deleteStaff = async (req: Request, res: Response, next: NextFunction) => {
  // Find & delete staff member
  const {id} = req.body;
  try{
    await Staff.destroy(
      {where: {id:id}}
    )
    res.send({status: 'ok'})
  }catch (e) {
    console.error('Delete staff error', e);
    next(new HttpException(HttpStatusCodes.InternalServerError, e));
    res.send({status: 'error'})
    }
};

export const newStaff = async (req: Request, res: Response, next: NextFunction) => {
  // Add new staff member
  const {email, role, studies, status} = req.body;
  let is_admin;
  let is_staff;
  let id;

  if(role === "admin"){
    is_admin = true;
    is_staff = false;
  } else if(role === "staff"){
    is_staff = true;
    is_admin = false;
  };

  try{
    await Staff.create(
      {password:"test", email:email, studies:studies, is_active:status, is_staff:is_staff, is_admin: is_admin}
    )
    res.send({status: 'ok'})
  }catch (e) {
    console.error('Add new staff error', e);
    next(new HttpException(HttpStatusCodes.InternalServerError, e));
    res.send({status: 'error'})
  }
};