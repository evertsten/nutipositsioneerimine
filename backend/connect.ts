const { Sequelize } = require('sequelize');

//database wide options
var opts = {
  define: {
      //prevent sequelize from pluralizing table names
      freezeTableName: true
  }
}

export default(db: string) => {
  const connect = async () => {
    const sequelize = new Sequelize(db, opts)
      try{
        await sequelize.authenticate();
        console.log('Connection with PostgreSQL has been established successfully.');
      } catch (error) {
      console.error('Unable to connect to the database:', error);
      }
    }
  connect();
}