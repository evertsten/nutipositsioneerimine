const { Sequelize, DataTypes } = require('sequelize');

//database wide options
var opts = {
    define: {
        //prevent sequelize from pluralizing table names
        freezeTableName: true,
        timestamps: false,
        returning: true
    }
}
const sequelize = new Sequelize('postgres://postgres:speedlink@0.0.0.0:5432/nutipos', opts);

const Staff = sequelize.define('staff', {
    // Model attributes are defined here
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    last_login: {
      type: 'TIMESTAMP',
      allowNull: true,
      timestamps: true
    },
    studies: {
      type: DataTypes.ARRAY(Sequelize.INTEGER),
      allowNull: true,
    },
    is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    is_staff: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    is_admin: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
  });

  export default Staff;