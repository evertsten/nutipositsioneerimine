export const environment = {
    production: true,
    tokenExpirationSeconds: {
      emailInvitation: 60 * 24 * 2, // 2 days
      accessToken: 60* 15, //  15 minutes
      refreshToken: 60 * 24 * 14 // 60 * 2 weeks
    },
    apiUrl: 'https://mingileht.ee/api',
    pageUrl: 'https://mingileht.ee'
  };