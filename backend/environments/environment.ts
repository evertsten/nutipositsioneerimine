import {RuntimeInfo} from '../utils/runtime_info';
import * as ProductionEnvironment from './environment.prod';

// TODO: move token expiration times here!

export const environment = {
  production: false,
  tokenExpirationSeconds: {
    emailInvitation: 60,
    accessToken: 600,
    refreshToken: 600 * 15
  },
  apiUrl: 'http://localhost:3000/api',
  pageUrl: 'http://localhost:4200'
};

export function initializeEnvironment(): void {
  if (!RuntimeInfo.isDevelopmentRun) {
    // swap environment out for production
    const productionEnvironment = ProductionEnvironment.environment as any;
    const devEnvironment = environment as any;

    for (const key of Object.keys(environment)) {
      devEnvironment[key] = productionEnvironment[key];
    }
    console.log(`[PRODUCTION] environment initialized`);
  } else {
    console.log(`[DEVELOPMENT] environment initialized`);
  }
}
