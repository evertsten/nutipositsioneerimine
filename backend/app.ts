import express from 'express';
import {Application} from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import connect from './connect';
import {RuntimeInfo} from './utils/runtime_info';
import {environment} from './environments/environment';
import * as StaffController from './controllers/staff_controller';

const app: Application = express();

const http = require('http').createServer(app);

http.listen(3000, () => {
  console.log('Server & websocket listening on: 3000');
  setInterval(function(){console.log("Keep VM running")},300000);
});

//Socket setup
const io = require('socket.io')(http, {
  cors: {
    origins: ['http://13.74.135.154:4200', 'http://localhost:4200']
  }
});

//Socket functions
io.on('connection', (socket) => {
  console.log('a user connected, id:', socket.id);
  io.emit('broadcast', `New device connected, id: ${socket.id}`)
  
  socket.on('disconnect', () => {
    console.log('user disconnected, id:', socket.id);
    io.emit('broadcast', `Device disconnected, id: ${socket.id}`)
  });

  socket.on('message', (msg) => {
    io.emit('broadcast', `${socket.id} says: ${msg}`)
  });
});

if (RuntimeInfo.isDevelopmentRun) {
    app.use((req, res, next) => {
      // Access-Control-Allow-Origin cannot be * when making http requests with withCredentials set to true.
      // widthCredentials need to be true to be able to use cookies!
      res.setHeader('Access-Control-Allow-Origin', '*');
      res.setHeader('Access-Control-Allow-Credentials', 'true');
      res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, XSRF-Token');
      res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
      next();
    });
  }

const bodyParsers = [bodyParser.json(), bodyParser.urlencoded({ extended: true })];

//Staff routes
app.get('/api/staff/getAll', bodyParsers, StaffController.allStaff);
app.post('/api/staff/updateOne', bodyParsers, StaffController.updateStaff);
app.post('/api/staff/deleteOne', bodyParsers, StaffController.deleteStaff);
app.post('/api/staff/new', bodyParsers, StaffController.newStaff);

//CONNECT TO POSTGRES DATABASE
const db = 'postgres://postgres:speedlink@0.0.0.0:5432/nutipos';

connect(db);

module.exports = app;